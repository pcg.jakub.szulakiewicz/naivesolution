﻿using Hack;
using System;
using System.Linq;

public class Student
{
    public int Identifier { get; }
    public SpecialisationCollection AllowedSpecialisations { get; } = new SpecialisationCollection();
    public Specialisation SelectedSpecialisation { get; private set; }
    public bool HasSpecialisation => SelectedSpecialisation != null;

    public Student(int identifier)
    {
        this.Identifier = identifier;
    }

    public void AddAllowedSpecialisation(Specialisation specialisation)
    {
        AllowedSpecialisations.Add(specialisation);
    }

    public bool SelectSpecialisation (Specialisation specialisation)
    {
        if (!AllowedSpecialisations.Any(spec => spec.Identifier == specialisation.Identifier))
            throw new Exception("SpecialisationNotAllowed");

        SelectedSpecialisation = specialisation;
        specialisation.AddStudent(this);
        return true;
    }

    internal void Clear()
    {
        this.SelectedSpecialisation = null;
    }
}
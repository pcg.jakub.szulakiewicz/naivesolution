﻿using Hack;
using System;
using System.Collections.Generic;

public class Specialisation
{
    public int Identifier { get; }
    public int MaxStudents { get; }

    public bool HasFreeSpace => MaxStudents > StudentsCount;

    public int StudentsCount => students.Count;

    public int CandidatesCount => Candidates.Count;

    public StudentCollection Candidates { get; private set; } = new StudentCollection();
    public object PlacesLeft => this.MaxStudents - StudentsCount;

    private StudentCollection students = new StudentCollection();

    public Specialisation(int identifier, int maxStudents)
    {
        this.Identifier = identifier;
        this.MaxStudents = maxStudents;
    }

    public void AddStudent(Student student)
    {
        if (students.Count < MaxStudents)
            this.students.Add(student);
        else throw new Exception("Max students exceed");
    }

    public void AddCandidate(Student student)
    {
        this.Candidates.Add(student);
    }

    internal void Clear()
    {
        this.students = new StudentCollection();
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Hack
{
    class Program
    {
        static SpecialisationCollection specialisations = new SpecialisationCollection();
        static StudentCollection students = new StudentCollection();

        static void Main(string[] args)
        {
            LoadData();

                       
            int bestResultSoFar = 0;

            for (int i = 0; i < 100000; i++)
            {
                students.Clear();
                specialisations.Clear();
                students.Shuffle();

                /// Wypełniamy kierunki, na które i tak jest mniej chętnych, niż miejsc:
                foreach (var specialisation in specialisations.Where(specialisation => specialisation.CandidatesCount < specialisation.MaxStudents))
                {
                    foreach (var student in specialisation.Candidates)
                        student.SelectSpecialisation(specialisation);
                }

                var remainingStudents = students.GetStudentsWithoutSpecialisation();
                remainingStudents.Shuffle();

                foreach (var student in remainingStudents)
                {
                    if (student.HasSpecialisation) continue;
                    var specialisation = student.AllowedSpecialisations.FirstOrDefault(spec => spec.HasFreeSpace);
                    if (specialisation == null)
                    {
                        //        Console.WriteLine("No space left for student {0}", student.Identifier);
                        continue;
                    }

                    student.SelectSpecialisation(specialisation);
                    //    Console.WriteLine("Student {0} match specialisation {1}", student.Identifier, specialisation.Identifier);
                }

                // Console.WriteLine("===================Summary===============");
                // Console.WriteLine("Total students: {0}", students.Count);

                var matches = students.Count(stud => stud.HasSpecialisation);
                Console.SetCursorPosition(0, 0);
                Console.WriteLine("Iteration: {0}, Matched students: {1}, Best so far: {2}", i, matches, bestResultSoFar);


                if (matches > bestResultSoFar)
                {
                    bestResultSoFar = matches;
                    using (var writer = new StreamWriter("solution.csv", false))
                    {
                        writer.WriteLine("studentId, specialisationId");
                        foreach (var student in students)
                            writer.WriteLine($"{student.Identifier}, {student.SelectedSpecialisation?.Identifier}");
                    }
                    Console.WriteLine("====================Matched students: {0}", matches);
                }
            }
        }

        private static void LoadData()
        {
            using (var reader = new StreamReader("source_data.csv"))
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(";");

                    var studentId = int.Parse(line[0]);
                    var specialisationId = int.Parse(line[1]);
                    var maxCapacity = int.Parse(line[2]);

                    var student = students.GetById(studentId);
                    if (student == null)
                    {
                        student = new Student(studentId);
                        students.Add(student);
                    }

                    var specialisation = specialisations.GetById(specialisationId);
                    if (specialisation == null)
                    {
                        specialisation = new Specialisation(specialisationId, maxCapacity);
                        specialisations.Add(specialisation);
                    }

                    student.AddAllowedSpecialisation(specialisation);
                    specialisation.AddCandidate(student);
                }
        }
    }
}

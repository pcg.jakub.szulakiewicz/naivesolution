Rozwiązanie "naiwne".


1. Najpierw wypełnia studentami te kierunki, na których jest więcej miejsc, niż kandydatów
2. Następnie pozostałych studentów losowo sortuje i po kolei przypisuje każdego z nich do pierwszego kierunku na który się zgłosili

Kroki 1 i 2 są powtarzane w pętli 100 000 razy, najlepszy rezultat jest zapisywany do pliku csv.
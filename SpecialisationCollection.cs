﻿using System.Collections.Generic;
using System.Linq;

namespace Hack
{
    public class SpecialisationCollection : List<Specialisation>
    {
        public SpecialisationCollection() : base()
        {
        }

        public Specialisation GetById(int id)
        {
            return this.FirstOrDefault(spec => spec.Identifier == id);
        }

        public new bool Add(Specialisation specialisation)
        {
            if (this.Any(spec => spec.Identifier == specialisation.Identifier))
                return false;

            base.Add(specialisation);
            return true;
        }

        public new void Clear()
        {
            foreach(var specialisation in this)
                specialisation.Clear();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hack
{
    public class StudentCollection : List<Student>
    {
        public StudentCollection() : base()
        {
        }

        public StudentCollection(IEnumerable<Student> collection) : base(collection)
        {
        }

        public Student GetById(int id)
        {
            return this.FirstOrDefault(student => student.Identifier == id);
        }


        public new bool Add(Student student)
        {
            if (this.Any(stud => stud.Identifier == student.Identifier))
                return false;

            base.Add(student);
            return true;
        }

        static Random rng = new Random();

        public void Shuffle()
        {
            int n = this.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                Student value = this[k];
                this[k] = this[n];
                this[n] = value;
            }
        }

        public new void Clear()
        { 
            foreach (var student in this)
            {
                student.Clear();
            }
        }

        public StudentCollection GetStudentsWithoutSpecialisation()
        {
            return new StudentCollection(this.Where(student => student.SelectedSpecialisation == null).ToList());
        }
    }
}